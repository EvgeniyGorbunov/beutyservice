// const express = require('express')
// const path = require('path')
// const PORT = process.env.PORT || 5000

// express()
//   .use(express.static(path.join(__dirname, 'public')))
//   .set('views', path.join(__dirname, 'views'))
//   .set('view engine', 'ejs')
//   .get('/', (req, res) => res.render('pages/index'))
//   .listen(PORT, () => console.log(`Listening on ${ PORT }`))
var express = require('express');
var app = express();
const PORT = process.env.PORT || 3000;

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var fs = require('fs');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var users = [];

function getUserList() {
  return new Promise((resolve, reject) => {
    fs.readFile('./assets/users.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function saveUsers() {
  fs.writeFile('./assets/users.json', JSON.stringify(users), () => { });
}

function getWorks(){
  return new Promise((resolve, reject)=>{
    fs.readFile('./assets/works.json', 'utf-8',(err,data)=>{
      if(!err){
        resolve(data);
      }else{
        reject();
      }
    })
  });
}
function getMasters(){
  return new Promise((resolve, reject)=>{
    fs.readFile('./assets/masters.json', 'utf-8',(err,data)=>{
      if(!err){
        resolve(data);
      }else{
        reject();
      }
    })
  });
}

app.get('/', (req, res) => {
  res.send(`'Api is work!'
    <button type="button" disabled>Нажать если паша вдруг так сделает</button>
    `);
});


app.post('/users', (req, res) => {
  const existUser = users.find(us => us.email === req.body.email);
  if (existUser) {
    res.sendStatus(500);
  } else {
    const user = {
      id: Date.now(),
      email: req.body.email,
      password: req.body.password
    };
    users.push(user);
    res.send(user);
    saveUsers();
  }
});

app.post('/auth', (req, res) => {
  const user = req.body;
  const existUser = users.find(u => (u.email === user.email && u.password === user.password));
  if (existUser) {
    res.send(existUser.id.toString());
  } else {
    res.sendStatus(500);
  }
});

app.get('/users', (req, res) => {
  const id = req.query.id;
  console.log(id);
  const user = users.find(u => +u.id === +id);
  if (user) {
    res.send(JSON.stringify(user.email));
  } else {
    res.sendStatus(500);
  }
});

app.get('/master', (req, res)=> {
  const serviceName = req.query.name;
  getWorks().then(works=>{
   const findWork = works.find(work=> work.name === serviceName);
   if(findWork){
     console.log(findWork);
   }
  });

});

app.listen(PORT, () => {
  console.log('Server started1w222w!');
  getUserList().then(data => users = JSON.parse(data));
});
