import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appServiceNamePipe'
})

export class ServiceNamePipe implements PipeTransform {
  transform(value: string) {
    switch (value) {
      case 'manicure':
        return 'Маникюр';
      case 'pedicure':
        return 'Педикюр';
      case 'brows':
        return 'Брови';
      case 'eyelashes':
        return 'Ресницы';
      default:
        return '';
    }
  }
}
