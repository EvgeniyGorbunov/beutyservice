import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { LoginService } from './login.service';

export class ParamInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const reqParams = req.clone({
      params: req.params.set('token', this.loginService.token)
    });
    return next.handle(reqParams);
  }
}
