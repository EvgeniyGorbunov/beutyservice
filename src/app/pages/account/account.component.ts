import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { DataService } from '@services/core';
import * as moment from 'moment';
import { Order } from 'src/app/models/data';

@Component({
  selector: 'app-account-page',
  templateUrl: 'account.component.html',
  styleUrls: ['account.component.scss']
})

export class AccountPageComponent implements OnInit {

  public email: string;

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Number';
  showYAxisLabel = true;
  yAxisLabel = 'Color Value';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  multi: any[] = [
    {
      name: 'Понедельник',
      value: 0,

    },
    {
      name: 'Вторник',
      value: 0,

    },
    {
      name: 'Среда',
      value: 0,

    },
    {
      name: 'Четверг',
      value: 0,

    },
    {
      name: 'Пятница',
      value: 0,

    }
  ];
  public isDataLoad: boolean;

  constructor(private loginService: LoginService, private dataService: DataService) { }

  ngOnInit() {
    this.getInfo();
    this.loadStats();
  }

  public logout(): void {
    this.loginService.logout();
  }

  private getInfo(): void {
    this.loginService.getUserInfo().then(result => {
      this.email = result;
    });

  }
  private loadStats(): void {
    this.dataService.getOrdersStats().then(data => {
      data.forEach((order, index) => {
        const searchDay = this.multi.find(day => day.name.toLowerCase() === this.convertToDay(order));
        if (searchDay) {
          searchDay.value++;
        }
        if (index === data.length - 1) {
          this.isDataLoad = true;
        }
      });
    });
  }
  private convertToDay(order: Order): string {
    moment.locale('ru');
    return moment.unix(order.date).format('dddd');

  }


}

