import { RouterModule } from '@angular/router';
import { AccountPageComponent } from './account.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [
    AccountPageComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccountPageComponent
      }
    ])
  ]
})

export class AccountPageModule {}
