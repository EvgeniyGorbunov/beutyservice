import { RouterModule } from '@angular/router';
import { PricePageComponent } from './price.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    PricePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PricePageComponent
      }
    ])
  ]
})

export class PricePageModule {}
