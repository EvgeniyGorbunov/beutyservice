import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContentPageComponent } from './content.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxFullCalendarModule } from 'ngx-fullcalendar';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';

@NgModule({
  declarations: [
    ContentPageComponent,
    ConfirmModalComponent
  ],
  imports: [
    CommonModule,
    NgxFullCalendarModule,
    PipesModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentPageComponent
      }
    ])
  ]
})

export class ContentPageModule {}
