import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: 'confirm-modal.component.html',
  styleUrls: ['confirm-modal.component.scss']
})

export class ConfirmModalComponent implements OnInit {

  public name: FormControl;
  public phone: FormControl;
  public confirmForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.createFormFields();
    this.createConfirmForm();
  }

  private createFormFields(): void {
    this.name = new FormControl('',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]);

    this.phone = new FormControl('+375',
      [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ]);
  }

  private createConfirmForm(): void {
    this.confirmForm = new FormGroup({
      name: this.name,
      phone: this.phone
    });
  }
}
