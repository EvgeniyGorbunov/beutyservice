import { AuthGuard } from '@services/core';
import { NgModule } from '@angular/core';
import { Route, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Route[] = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full'
  },
  {
    path: 'about',
    loadChildren: './pages/about/about.module#AboutPageModule'
  },
  {
    path: 'price',
    loadChildren: () => import('./pages/price/price.module').then(m => m.PricePageModule)
  },
  {
    path: 'content/:type',
    loadChildren: './pages/content/content.module#ContentPageModule'
  },
  {
    path: 'account',
    loadChildren: './pages/account/account.module#AccountPageModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      anchorScrolling: 'enabled',
      scrollPositionRestoration: 'enabled',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
