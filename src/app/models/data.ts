export interface Master {
  id: number;
  name: string;
  portf: Portfolio;
}

export interface Portfolio {
  text: string;
  images: string[];
}

export interface Work {
  id: number;
  name: string;
  masters: number[];
  time: number;
  price: number;
}

export interface Order {
  order_id?: number;
  date: number;
  work_id: number;
  client_id: number;
  master_id: number;
  options: number[];
  time: number;
}

export interface Option {
  option_id: number;
  work_id: number;
  price: number;
  name: string;
  time: number;
}

export interface ScheduleItem {
  time: number;
  is_reserved: boolean;
  order_id?: number;
}
