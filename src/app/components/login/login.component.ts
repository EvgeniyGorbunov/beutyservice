import { User } from '@models/core';
import { LoginService } from '@services/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {

  public isRegistration: boolean;

  public email: FormControl;
  public password: FormControl;
  public confirm: FormControl;
  public userForm: FormGroup;

  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit() {
    this.createFormFields();
    this.createUserForm();
  }

  public closeModal(): void {
    this.loginService.closeModal();
  }

  public toggleState(): void {
    this.createFormFields();
    this.createUserForm();
    this.isRegistration = !this.isRegistration;
  }

  public registration(): void {
    if (this.userForm.valid) {
      const user: User = {
        email: this.email.value,
        password: this.password.value
      };
      this.loginService.register(user).then(res => {
        this.closeModal();
        this.router.navigate(['account']);
      });
    }
  }

  public auth(): void {
    this.confirm.setValue(this.password.value);
    if (this.userForm.valid) {
      const user: User = {
        email: this.email.value,
        password: this.password.value
      };
      this.loginService.authorization(user).then(_ => {
        this.closeModal();
        this.router.navigate(['account']);
      });
    }
  }

  private createFormFields(): void {
    this.email = new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(25), Validators.email]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]);
    this.confirm = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]);
  }

  private createUserForm(): void {
    this.userForm = new FormGroup({
      email: this.email,
      password: this.password,
      confirm: this.confirm
    });
  }
}
